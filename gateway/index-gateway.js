const express = require('express');
const http = require('http');

const app = express();
//creamos un server de http habilitado con espress
const server = http.createServer( app );

const { Server } = require('socket.io');
//creamos una nueva constaste para instaciar el servidor http
const io = new Server( server );
//solicitamos el servicio de la api con bull
const api = require('../api/test')

const port = 80;
//inicializamos el server
server.listen( port , () => {
    console.log("servidor inicializado", port);

    io.on('connection', ( socket ) => {

        console.log( "new connection", socket.id );

        socket.on('req:microservice:view', async({ }) => {

            try {

                console.log('req:microservice:view');
                
                const { statusCode, data, message } = await api.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)
                
            }
            
        });

        socket.on('req:microservice:create', async({ name, age, color }) => {

           try {
            
                console.log('req:microservice:create', ({ name, age, color }));

                const { statusCode, data, message } = await api.Create({ name, age, color });

                return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });

           } catch (error) {
                console.log(error)
           }
        });

        socket.on('req:microservice:delete', async({ id }) => {

            try {

                console.log('req:microservice:delete', ({ id }));

                const { statusCode, data, message } = await api.Delete({ id });

                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:update', async({ age, color, id, name }) => {

            try {

                console.log('req:microservice:update', ({ age, color, id, name }));

                const { statusCode, data, message } = await api.Update({ age, color, id, name });

                return io.to(socket.id).emit('res:microservice:update', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:findOne', async({ id }) => {
            
            try {

                console.log('req:microservice:findOne', ({ id }));

                const { statusCode, data, message } = await api.FindOne({ id });

                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        })
    });
});