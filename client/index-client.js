const { io } = require('socket.io-client');

// const socket = io('http://192.168.1.105', { port: 80 });
const socket = io('http://192.168.1.105');

const main = async() => {
    try {
        
        setTimeout(() => { console.log( socket.id ) }, 500);

        //servcio que escucha
        socket.on('res:microservice:view', ({ statusCode, data, message }) => {

            console.log('res:microservice:view', { statusCode, data, message });
        });

        socket.on('res:microservice:create', ({ statusCode, data, message }) => {

            console.log('res:microservice:create', { statusCode, data, message });
        
        });

        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {

            console.log('res:microservice:delete', ({ statusCode, data, message }));

        });

        socket.on('res:microservice:update', ({ statusCode, data, message}) => {

            console.log('res:microservice:update', ({ statusCode, data, message }));

        });

        socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {

            console.log('res:microservice:findOne', ({ statusCode, data, message }));
        });

        // setInterval(() => {socket.emit('req:microservice:view', ({}), 350);
        setInterval(() => {
        //EJECUTAR Y DESCOMENTAR DE A CUERDO A LA NECESIDAD  SERVICIO QUE EMITE

            //socket.emit('req:microservice:create', ({ name:'andres', age: 22, color:'Morado' }));
                
            //socket.emit('req:microservice:delete',({ id:1}));

            //socket.emit('req:microservice:update', ({ id: 24, name:'andres', age: 27, color:'verde'}));

            //socket.emit('req:microservice:findOne', ({ id: 25 }));

            //socket.emit('req:microservice:view', ({}));

        }, 500);


    } catch (error) {
        console.log(error);
    }
}

main();