const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');


//modelo 
const Model = sequelize.define('Curso', {

    name: { type: DataTypes.STRING },

    age: { type: DataTypes.BIGINT },

    color: {type: DataTypes.STRING }

});

//iniciarlizar el model 
const syncroDB = async() => {

    try {
        console.log('vamos a inciar base de datos')
        
        // detnemos el registro de logueo { logging: false }
        await Model.sync({ logging: false })

        console.log('base de datos iniciada')

        return { statusCode: 200, data: 'ok'}
 
    } catch (error) {
         console.log( error );
 
         return { statusCode: 500, message: error.toString()}
 
    }
}


module.exports = { Model, syncroDB };