import React, { useEffect, useState } from "react";
import { io } from "socket.io-client";

function App() {
  //definimos para setear
  const [id, setId] = useState();
  const [data, setData] = useState([]);

  useEffect(() => {
    //renderiza al inicio
    const socket = io("http://localhost", {
      transports: ["websocket"],
      jsonp: false,
    });

    setTimeout(() => setId(socket.id), 500);
    //servcio que escucha
    socket.on("res:microservice:view", ({ statusCode, data, message }) => {
      console.log("res:microservice:view", { statusCode, data, message });
      // datos recibiendo
      console.log({ statusCode, data, message });

      if (statusCode === 200) { setData(data)}
    
    });

    socket.on("res:microservice:create", ({ statusCode, data, message }) => {
      console.log("res:microservice:create", { statusCode, data, message });
    });

    socket.on("res:microservice:delete", ({ statusCode, data, message }) => {
      console.log("res:microservice:delete", { statusCode, data, message });
    });

    socket.on("res:microservice:update", ({ statusCode, data, message }) => {
      console.log("res:microservice:update", { statusCode, data, message });
    });

    socket.on("res:microservice:findOne", ({ statusCode, data, message }) => {
      console.log("res:microservice:findOne", { statusCode, data, message });
    });

    //setInterval(() => {
      //EJECUTAR Y DESCOMENTAR DE A CUERDO A LA NECESIDAD

      //socket.emit('req:microservice:create', ({ name:'leonard', age: 22, color:'Morado' }));

      //socket.emit('req:microservice:delete',({ id:23}));

      //socket.emit('req:microservice:update', ({ id: 24, name:'andres', age: 27, color:'verde'}));

      //socket.emit('req:microservice:findOne', ({ id: 25 }));

      socket.emit("req:microservice:view", {});

  }, []);

  return (
    <div>
      <p>{id ? `Estas en linea ${id}` : "Fuera de linea"}</p>
      { data.map((v, i) => <p key={i} >Nombre: {v.name} Edad: {v.age} {v.worker}</p> )}
    </div>
  );
}

export default App;
