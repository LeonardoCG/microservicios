const { internalError } = require('../settings')
const Services = require('../Services/services');

const { 
    queueCreate, 
    queueDelete, 
    queueUpdate, 
    queueFindOne, 
    queueView } = require('./adapters');

//adaptador || interno

const Create = async(job, done) => {

    try {

        const { name, age, color } = job.data;

        let { statusCode, data, message }  = await Services.Create({ name, age, color });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueCreate', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Delete = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.Delete({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueDelete', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Update = async(job, done) => {

    try {

        const { age, color, id, name } = job.data;

        let { statusCode, data, message }  = await Services.Update({ age, color, id, name });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueDelete', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const FindOne = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueFindOne', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const View = async(job, done) => {

    try {

        const {} = job.data;

        console.log(job.id)

        let { statusCode, data, message }  = await Services.View({});
        //vamos a mapear la data y asignar el id del worker para ver quien hizo la consulta
        // console.log(data.map(v => ({...v.toJSON(), ...{worker: job.id}})))
        done(null, { statusCode, data: data.map(v => ({...v.toJSON(), ...{worker: job.id}})), message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueView', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const run = async() => {
    
    try {
        console.log("vamos a inicializar el worker")

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueUpdate.process(Update);

        queueFindOne.process(FindOne);

        queueView.process(View);
        
    } catch (error) {
        console.log(error)
        
    }
}
 
module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View,
    run
}