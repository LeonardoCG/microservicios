// libreria npm i bull
const bull = require('bull');

const redis = { host: '192.168.1.105' , port: 6379 }

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull("curso:create", opts);

const queueDelete = bull("curso:delete", opts);

const queueUpdate = bull("curso:update", opts);

const queueFindOne = bull("curso:findOne", opts);

const queueView = bull("curso:view", opts);


const Create = async({ age, color, name }) => {

    try {
        const job = await queueCreate.add({ age, color, name });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const Delete = async({ id }) => {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }
        

    } catch (error) {
        console.log(error)
    }

}

const Update = async({ age, color, id, name }) => {

    try {
        const job = await queueUpdate.add({ age, color, id, name });

        const { statusCode, data, message } = await job.finished(); 

       return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const FindOne = async({ id }) => {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const View = async({ }) => {

    try {

        const job = await queueView.add({});

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

module.exports =  { Create, Delete, Update, FindOne, View }